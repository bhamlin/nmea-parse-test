use super::types::{NmeaParseFailure, NmeaParseTinyFailure};
use chrono::LocalResult::Single;
use chrono::{DateTime, Local, TimeZone, Utc};
use std::ops::{BitAnd, BitXor};
use std::{fmt::Display, str::FromStr};

pub struct ParseTool {
    line_type: String,
    raw_line: Option<String>,
}

impl ParseTool {
    pub fn for_line(line_type: &str, line: &str) -> Self {
        Self {
            line_type: String::from(line_type),
            raw_line: Some(String::from(line)),
        }
    }

    pub fn parse_to<T>(&self, input: &str, default: T) -> Result<T, NmeaParseFailure>
    where
        T: FromStr,
        T: Display,
    {
        if input.is_empty() {
            Ok(default)
        } else {
            input.parse::<T>().map_err(|_| NmeaParseFailure {
                status: String::from("Convertion failure"),
                line_type: self.line_type.clone(),
                message: format!("Error converting {}", input),
                raw_line: self.raw_line.clone(),
            })
        }
    }

    pub fn parse_date_time(
        &self,
        date: &str,
        time: &str,
        // One day add enum for date order
    ) -> Result<DateTime<Local>, NmeaParseFailure> {
        let date_num_parts = packed_str_to_u::<u16>(date, 2) //
            .map_err(|e| NmeaParseFailure {
                status: String::from("Failed parsing as u32"),
                line_type: self.line_type.clone(),
                message: e.message,
                raw_line: self.raw_line.clone(),
            })?;
        let year = i32::from(date_num_parts[2]) + 2000i32;
        let month = u32::from(date_num_parts[0]);
        let day = u32::from(date_num_parts[1]);

        let time_num_parts = packed_str_to_u(time, 2) //
            .map_err(|e| NmeaParseFailure {
                status: String::from("Failed parsing as u32"),
                line_type: self.line_type.clone(),
                message: e.message,
                raw_line: self.raw_line.clone(),
            })?;

        let utc_time = match Utc.with_ymd_and_hms(
            year,
            month,
            day,
            time_num_parts[0],
            time_num_parts[1],
            time_num_parts[2],
        ) {
            Single(time) => time,
            _ => Utc::now(),
        };
        let fix_time = utc_time.with_timezone(&Local);

        Ok(fix_time)
    }

    pub fn parse_time(&self, time: &str) -> Result<DateTime<Local>, NmeaParseFailure> {
        let date_str = &Local::now().format("%m%d%y").to_string();

        self.parse_date_time(date_str, time)
    }
}

pub fn packed_str_to_u<T: FromStr>(
    chonk: &str,
    chunk_size: usize,
) -> Result<Vec<T>, NmeaParseTinyFailure> {
    let chunks = chonk.as_bytes().chunks(chunk_size);
    let parts = chunks
        .map(|c| String::from_utf8(c.to_vec()))
        .collect::<Result<Vec<String>, _>>()
        .map_err(|e| NmeaParseTinyFailure {
            message: e.to_string(),
        })?;
    let int_parts = parts
        .iter()
        .map(|i| i.parse::<T>())
        .collect::<Result<Vec<T>, _>>()
        .map_err(|_| NmeaParseTinyFailure {
            message: String::from("failed parsing to number"),
        })?;

    Ok(int_parts)
}

pub fn test_line_crc(line_type: &str, line: &str, crc: &str) -> Result<(), NmeaParseFailure> {
    let mut csum: u8 = 0;
    line.as_bytes()
        .iter()
        .for_each(|v| csum = csum.bitxor(v).bitand(0xff));

    match format!("{:02X}", csum).eq(crc) {
        true => Ok(()),
        false => Err(NmeaParseFailure {
            status: String::from("CRC"),
            line_type: String::from(line_type),
            message: String::from("Failure checking line CRC"),
            raw_line: Some(line.to_string()),
        }),
    }
}

pub fn dmd_to_nad83(dmd: f64) -> f64 {
    let is_neg = if dmd < 0.0f64 { -1.0f64 } else { 1.0f64 };
    let pdmd = f64::abs(dmd);
    let degs = f64::floor(pdmd / 100.0f64);
    let mins = f64::floor(pdmd) % 100.0f64;
    let sdec = pdmd.fract() * 100.0f64;

    is_neg * (degs + (mins / 60.0f64) + (sdec / 3600.0f64))
}

pub fn gps_position(slice: &[&str]) -> (f64, f64) {
    let v_str = slice[0];
    let v_sign = slice[1];
    let u_str = slice[2];
    let u_sign = slice[3];

    let lat_sign = if "S".eq(v_sign) { -1.0f64 } else { 1.0f64 };
    let lon_sign = if "W".eq(u_sign) { -1.0f64 } else { 1.0f64 };
    let latitude = dmd_to_nad83(v_str.parse().unwrap_or(0.0f64) * lat_sign);
    let longitude = dmd_to_nad83(u_str.parse().unwrap_or(0.0f64) * lon_sign);

    (latitude, longitude)
}
