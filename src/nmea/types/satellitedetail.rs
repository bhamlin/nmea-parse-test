use std::fmt::Display;

use super::{NmeaLine, NmeaParseFailure};
use crate::nmea::tools::ParseTool;

pub struct SatelliteDetailEntry {
    prn: u16,
    elevation: i16,
    azimuth: i16,
    snr: u16,
}

impl Display for SatelliteDetailEntry {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "[{}: e{} a{} snr{}]",
            self.prn, self.elevation, self.azimuth, self.snr
        )
    }
}

pub struct SatellitesInView {
    detail_count: u8,
    detail_index: u8,
    satellite_count: u8,
    satellites: Vec<SatelliteDetailEntry>,
    uid: Option<String>,
}

impl SatellitesInView {
    pub fn sattelite_stream_string(&self) -> String {
        self.satellites
            .iter()
            .map(|s| s.to_string())
            .collect::<Vec<String>>()
            .join(",")
    }
}

impl Display for SatellitesInView {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "dtl: line {} of {} {}:[{}]",
            self.detail_index,
            self.detail_count,
            self.satellite_count,
            self.sattelite_stream_string()
        )
    }
}

impl NmeaLine for SatellitesInView {
    fn parse(
        type_str: &str,
        line: &str,
        parts: &[&str],
        uid: Option<String>,
    ) -> Result<Self, NmeaParseFailure>
    where
        Self: Sized,
    {
        let parse_tool = ParseTool::for_line(type_str, line);

        let detail_count = parse_tool.parse_to(parts[1], 0)?;
        let detail_index = parse_tool.parse_to(parts[2], 0)?;
        let satellite_count = parse_tool.parse_to(parts[3], 0)?;

        let total = parts.len();
        let count = (total - 4) / 4;
        let mut satellites = Vec::<SatelliteDetailEntry>::new();
        if total > 4 {
            for n in 0..count {
                let i = (n + 1) * 4;

                #[allow(clippy::identity_op)]
                satellites.push(SatelliteDetailEntry {
                    prn: parse_tool.parse_to(parts[i + 0], 0)?,
                    elevation: parse_tool.parse_to(parts[i + 1], 0)?,
                    azimuth: parse_tool.parse_to(parts[i + 2], 0)?,
                    snr: parse_tool.parse_to(parts[i + 3], 0)?,
                });
            }
        }

        Ok(SatellitesInView {
            detail_count,
            detail_index,
            satellite_count,
            satellites,
            uid,
        })
    }

    // #[cfg(debug_assertions)]
    // fn hidden(&self) -> bool {
    //     true
    // }

    fn get_uid(&self) -> Option<String> {
        Some(self.uid.as_ref().unwrap().clone())
    }
}
