use super::{FixQuality, NavigationTalkers, NmeaLine, NmeaParseFailure};
use crate::nmea::tools::{gps_position, ParseTool};
use chrono::{DateTime, Local};
use std::{collections::HashMap, fmt::Display};

#[allow(unused)]
#[derive(Debug)]
pub struct NavSatSystem {
    fix_time: DateTime<Local>,
    latitude: f64,
    longitude: f64,
    systems_used: HashMap<NavigationTalkers, FixQuality>,
    count_sv: u8,
    hdop: f64,
    height: f64,
    uid: Option<String>,
}

impl NavSatSystem {
    pub fn systems_used_str(&self) -> String {
        self.systems_used
            .iter()
            .map(|(navigation_talkers, fix_quality)| {
                format!("{}: {}", navigation_talkers, fix_quality)
            })
            .collect::<Vec<String>>()
            .join(",")
    }
}

impl Display for NavSatSystem {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "nss: ({:.6}, {:.6}) @ {} [{}]",
            self.latitude,
            self.longitude,
            self.fix_time,
            self.systems_used_str(),
        )
    }
}

impl NmeaLine for NavSatSystem {
    fn parse(
        type_str: &str,
        line: &str,
        parts: &[&str],
        uid: Option<String>,
    ) -> Result<Self, NmeaParseFailure>
    where
        Self: Sized,
    {
        let parse_tool = ParseTool::for_line(type_str, line);

        let fix_time = parse_tool.parse_time(&parts[1][..6])?;
        let (latitude, longitude) = gps_position(&parts[2..6]);
        let count_sv = parse_tool.parse_to(parts[7], 0u8)?;
        let hdop = parse_tool.parse_to(parts[8], 0.0f64)?;
        let height = parse_tool.parse_to(parts[9], 0.0f64)?;

        let mut systems_used = HashMap::<NavigationTalkers, FixQuality>::new();
        parts[6].chars().enumerate().for_each(|(index, ch)| {
            match index {
                0 => systems_used.insert(NavigationTalkers::Gps, FixQuality::from(ch)),
                1 => systems_used.insert(NavigationTalkers::Glonass, FixQuality::from(ch)),
                2 => systems_used.insert(NavigationTalkers::Galileo, FixQuality::from(ch)),
                3 => systems_used.insert(NavigationTalkers::BeiDou, FixQuality::from(ch)),
                4 => systems_used.insert(NavigationTalkers::Qzss, FixQuality::from(ch)),
                _ => None,
            };
        });

        Ok(Self {
            fix_time,
            latitude,
            longitude,
            systems_used,
            count_sv,
            hdop,
            height,
            uid,
        })
    }

    // #[cfg(debug_assertions)]
    // fn hidden(&self) -> bool {
    //     true
    // }

    fn get_uid(&self) -> Option<String> {
        Some(self.uid.as_ref().unwrap().clone())
    }
}
