use std::fmt::Display;

use super::{NmeaLine, NmeaParseFailure};
use crate::nmea::tools::{gps_position, ParseTool};
use chrono::{DateTime, Local};

pub struct RecommendedMinimum {
    fix_time: DateTime<Local>,
    // status_active: bool,
    latitude: f64,
    longitude: f64,
    speed_knots: f64,
    track_angle: f64,
    mag_var: f64,
    uid: Option<String>,
}

impl Display for RecommendedMinimum {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "rmc: ({:.6}, {:.6}) @ {} [{}kts {}deg ({})]",
            self.latitude,
            self.longitude,
            self.fix_time,
            self.speed_knots,
            self.track_angle,
            self.mag_var,
        )
    }
}

impl NmeaLine for RecommendedMinimum {
    fn parse(
        type_str: &str,
        line: &str,
        parts: &[&str],
        uid: Option<String>,
    ) -> Result<Self, NmeaParseFailure>
    where
        Self: Sized,
    {
        let parse_tool = ParseTool::for_line(type_str, line);

        let fix_time = parse_tool.parse_date_time(&parts[9][..6], &parts[1][..6])?;
        // let status_active = "A".eq(parts[2]);

        let (latitude, longitude) = gps_position(&parts[3..7]);
        let speed_knots = parse_tool.parse_to(parts[7], 0.0f64)?;
        let track_angle = parse_tool.parse_to(parts[8], 0.0f64)?;

        let mag_var_sign = if "W".eq(parts[10]) { -1.0f64 } else { 1.0f64 };
        let mag_var = parse_tool.parse_to(parts[9], 0.0f64)? * mag_var_sign;

        Ok(RecommendedMinimum {
            fix_time,
            // status_active,
            latitude,
            longitude,
            speed_knots,
            track_angle,
            mag_var,
            uid,
        })
    }

    // #[cfg(debug_assertions)]
    // fn hidden(&self) -> bool {
    //     true
    // }

    fn get_uid(&self) -> Option<String> {
        Some(self.uid.as_ref().unwrap().clone())
    }
}
