use std::fmt::Display;

use super::{NmeaLine, NmeaParseFailure};

pub struct IgnoreMe {
    raw_line: String,
    uid: Option<String>,
}

impl Display for IgnoreMe {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.raw_line)
    }
}

impl NmeaLine for IgnoreMe {
    fn parse(
        _type_str: &str,
        line: &str,
        _parts: &[&str],
        uid: Option<String>,
    ) -> Result<Self, NmeaParseFailure>
    where
        Self: Sized,
    {
        Ok(Self {
            raw_line: String::from(line),
            uid,
        })
    }

    // #[cfg(debug_assertions)]
    // fn hidden(&self) -> bool {
    //     true
    // }

    #[cfg(not(debug_assertions))]
    fn hidden(&self) -> bool {
        true
    }

    fn get_uid(&self) -> Option<String> {
        Some(self.uid.as_ref().unwrap().clone())
    }
}
