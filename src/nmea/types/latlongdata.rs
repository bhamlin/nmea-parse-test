use std::fmt::Display;

use super::{NmeaLine, NmeaParseFailure};
use crate::nmea::tools::{gps_position, ParseTool};
use chrono::{DateTime, Local};

#[derive(Debug)]
pub struct LatLongData {
    latitude: f64,
    longitude: f64,
    fix_time: DateTime<Local>,
    uid: Option<String>,
}

impl Display for LatLongData {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "pos: ({:.6}, {:.6}) @ {}",
            self.latitude, self.longitude, self.fix_time
        )
    }
}

impl NmeaLine for LatLongData {
    fn parse(
        type_str: &str,
        line: &str,
        parts: &[&str],
        uid: Option<String>,
    ) -> Result<Self, NmeaParseFailure>
    where
        Self: Sized,
    {
        let parse_tool = ParseTool::for_line(type_str, line);

        let (latitude, longitude) = gps_position(&parts[1..5]);
        let fix_time = parse_tool.parse_time(&parts[5][..6])?;

        Ok(Self {
            latitude,
            longitude,
            fix_time,
            uid,
        })
    }

    // #[cfg(debug_assertions)]
    // fn hidden(&self) -> bool {
    //     true
    // }

    fn get_uid(&self) -> Option<String> {
        Some(self.uid.as_ref().unwrap().clone())
    }
}
