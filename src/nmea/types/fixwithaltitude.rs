use std::fmt::Display;

use super::{FixQuality, NmeaLine, NmeaParseFailure};
use crate::nmea::tools::{gps_position, ParseTool};
use chrono::{DateTime, Local};

#[allow(unused)]
#[derive(Debug)]
pub struct FixWithAltitude {
    latitude: f64,
    longitude: f64,
    fix_time: DateTime<Local>,
    fix_quality: FixQuality,
    satellite_count: u16,
    hdop: f64,
    altitude: f64,
    altitude_units: String,
    geoid: f64,
    geoid_units: String,
    dgps_update: u16,
    dgps_station: u16,
    uid: Option<String>,
}

impl Display for FixWithAltitude {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "fix: ({}, {}) @ {} elevation {} {}",
            self.latitude, self.longitude, self.fix_time, self.altitude, self.altitude_units
        )
    }
}

impl NmeaLine for FixWithAltitude {
    fn parse(
        type_str: &str,
        line: &str,
        parts: &[&str],
        uid: Option<String>,
    ) -> Result<Self, NmeaParseFailure>
    where
        Self: Sized,
    {
        let parse_tool = ParseTool::for_line(type_str, line);

        let (latitude, longitude) = gps_position(&parts[2..6]);

        let fix_time = parse_tool.parse_time(&parts[1][..6])?;
        let fix_quality_value = parse_tool.parse_to(parts[6], 0u8)?;
        let fix_quality = FixQuality::from(fix_quality_value);
        let satellite_count = parse_tool.parse_to(parts[7], 0u16)?;
        let hdop = parse_tool.parse_to(parts[8], 0.0f64)?;

        let altitude = parse_tool.parse_to(parts[9], 0.0f64)?;
        let altitude_units = String::from(parts[10]);
        let geoid = parse_tool.parse_to(parts[11], 0.0f64)?;
        let geoid_units = String::from(parts[12]);
        let dgps_update = parse_tool.parse_to(parts[13], 0u16)?;
        let dgps_station = parse_tool.parse_to(parts[14], 0u16)?;

        Ok(Self {
            latitude,
            longitude,
            fix_time,
            fix_quality,
            satellite_count,
            hdop,
            altitude,
            altitude_units,
            geoid,
            geoid_units,
            dgps_update,
            dgps_station,
            uid,
        })
    }

    // #[cfg(debug_assertions)]
    // fn hidden(&self) -> bool {
    //     true
    // }

    fn get_uid(&self) -> Option<String> {
        Some(self.uid.as_ref().unwrap().clone())
    }
}
