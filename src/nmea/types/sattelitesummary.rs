use std::fmt::Display;

use super::{NmeaLine, NmeaParseFailure};
use crate::nmea::tools::ParseTool;

pub enum SatteliteFixType {
    FixNone,
    Fix2d,
    Fix3d,
}

impl Display for SatteliteFixType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::FixNone => "NoFix",
            Self::Fix2d => "2dFix",
            Self::Fix3d => "3dFix",
        };

        write!(f, "{}", message)
    }
}

pub struct SatelliteSummary {
    fix_setting: String,
    fix_type: SatteliteFixType,
    satellites: Vec<u16>,
    precision: f64,
    hdop: f64,
    vdop: f64,
    uid: Option<String>,
}

impl Display for SatelliteSummary {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "smr: {}-{} P{} ({}, {}) {:?}",
            self.fix_setting, self.fix_type, self.precision, self.hdop, self.vdop, self.satellites
        )
    }
}

impl NmeaLine for SatelliteSummary {
    fn parse(
        type_str: &str,
        line: &str,
        parts: &[&str],
        uid: Option<String>,
    ) -> Result<Self, NmeaParseFailure>
    where
        Self: Sized,
    {
        let parse_tool = ParseTool::for_line(type_str, line);

        let fix_setting = parts[1].to_string();
        let fix_type = match parts[2] {
            "3" => SatteliteFixType::Fix3d,
            "2" => SatteliteFixType::Fix2d,
            _ => SatteliteFixType::FixNone,
        };
        // let mut satellites = Vec::<u16>::new();
        let satellites = parts
            .iter()
            .take(15)
            .skip(3)
            .filter_map(|prn| {
                if !prn.is_empty() {
                    parse_tool.parse_to(prn, 0).ok()
                } else {
                    None
                }
            })
            .collect();
        let precision = parse_tool.parse_to(parts[15], 0.0f64)?;
        let hdop = parse_tool.parse_to(parts[16], 0.0f64)?;
        let vdop = parse_tool.parse_to(parts[17], 0.0f64)?;

        Ok(SatelliteSummary {
            fix_setting,
            fix_type,
            satellites,
            precision,
            hdop,
            vdop,
            uid,
        })
    }

    // #[cfg(debug_assertions)]
    // fn hidden(&self) -> bool {
    //     true
    // }

    fn get_uid(&self) -> Option<String> {
        Some(self.uid.as_ref().unwrap().clone())
    }
}
