use std::fmt::Display;

use super::{NmeaLine, NmeaParseFailure};
use crate::nmea::tools::ParseTool;

pub struct Velocity {
    track_true: f64,
    track_mag: f64,
    speed_knots: f64,
    speed_kph: f64,
    uid: Option<String>,
}

impl Display for Velocity {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "vel: {}kph ({}kts) [{}t/{}m]",
            self.speed_kph, self.speed_knots, self.track_true, self.track_mag,
        )
    }
}

impl NmeaLine for Velocity {
    fn parse(
        type_str: &str,
        line: &str,
        parts: &[&str],
        uid: Option<String>,
    ) -> Result<Self, NmeaParseFailure>
    where
        Self: Sized,
    {
        let parse_tool = ParseTool::for_line(type_str, line);

        let track_true = parse_tool.parse_to(parts[1], 0.0f64)?;
        let track_mag = parse_tool.parse_to(parts[3], 0.0f64)?;
        let speed_knots = parse_tool.parse_to(parts[5], 0.0f64)?;
        let speed_kph = parse_tool.parse_to(parts[7], 0.0f64)?;

        Ok(Velocity {
            track_true,
            track_mag,
            speed_knots,
            speed_kph,
            uid,
        })
    }

    // #[cfg(debug_assertions)]
    // fn hidden(&self) -> bool {
    //     true
    // }

    fn get_uid(&self) -> Option<String> {
        Some(self.uid.as_ref().unwrap().clone())
    }
}
