use std::fmt::Display;

use super::{NmeaLine, NmeaParseFailure};
use crate::nmea::tools::ParseTool;
use chrono::{DateTime, Local};

pub struct DateAndTime {
    fix_time: DateTime<Local>,
    uid: Option<String>,
}

impl Display for DateAndTime {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "dnt: {}", self.fix_time)
    }
}

impl NmeaLine for DateAndTime {
    fn parse(
        type_str: &str,
        line: &str,
        parts: &[&str],
        uid: Option<String>,
    ) -> Result<Self, NmeaParseFailure>
    where
        Self: Sized,
    {
        let parse_tool = ParseTool::for_line(type_str, line);

        let year_len = parts[4].len();
        let year_two = &parts[4][year_len - 2..];
        let date_str = format!("{}{}{}", parts[2], parts[3], year_two);
        let fix_time = parse_tool.parse_date_time(&date_str, &parts[1][..6])?;

        Ok(DateAndTime { fix_time, uid })
    }

    // #[cfg(debug_assertions)]
    // fn hidden(&self) -> bool {
    //     true
    // }

    fn get_uid(&self) -> Option<String> {
        Some(self.uid.as_ref().unwrap().clone())
    }
}
