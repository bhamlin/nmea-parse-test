use std::fmt::Display;

use super::{NmeaLine, NmeaParseFailure};
use crate::nmea::tools::ParseTool;
use chrono::{DateTime, Local};

#[derive(Debug)]
pub struct PositionError {
    fix_time: DateTime<Local>,
    rms: f64,
    error_ellipse_v: f64,
    error_ellipse_u: f64,
    error_ellipse_angle: f64,
    latitude_error: f64,
    longitude_error: f64,
    height_error: f64,
    uid: Option<String>,
}

impl Display for PositionError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "err: {} rms:{} [{}, {} / {}] ({}, {}, {})",
            self.fix_time,
            self.rms,
            self.error_ellipse_u,
            self.error_ellipse_v,
            self.error_ellipse_angle,
            self.latitude_error,
            self.longitude_error,
            self.height_error,
        )
    }
}

impl NmeaLine for PositionError {
    fn parse(
        type_str: &str,
        line: &str,
        parts: &[&str],
        uid: Option<String>,
    ) -> Result<Self, NmeaParseFailure>
    where
        Self: Sized,
    {
        let parse_tool = ParseTool::for_line(type_str, line);

        let fix_time = parse_tool.parse_time(&parts[1][..6])?;
        let rms = parse_tool.parse_to(parts[2], 0.0f64)?;
        let error_ellipse_u = parse_tool.parse_to(parts[3], 0.0f64)?;
        let error_ellipse_v = parse_tool.parse_to(parts[4], 0.0f64)?;
        let error_ellipse_angle = parse_tool.parse_to(parts[5], 0.0f64)?;
        let latitude_error = parse_tool.parse_to(parts[6], 0.0f64)?;
        let longitude_error = parse_tool.parse_to(parts[7], 0.0f64)?;
        let height_error = parse_tool.parse_to(parts[8], 0.0f64)?;

        Ok(Self {
            fix_time,
            rms,
            error_ellipse_v,
            error_ellipse_u,
            error_ellipse_angle,
            latitude_error,
            longitude_error,
            height_error,
            uid,
        })
    }

    // #[cfg(debug_assertions)]
    // fn hidden(&self) -> bool {
    //     true
    // }

    fn get_uid(&self) -> Option<String> {
        Some(self.uid.as_ref().unwrap().clone())
    }
}
