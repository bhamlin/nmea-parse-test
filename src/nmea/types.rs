pub mod dateandtime;
pub mod fixwithaltitude;
pub mod ignoreme;
pub mod latlongdata;
pub mod navsat;
pub mod positionerror;
pub mod recmin;
pub mod satellitedetail;
pub mod sattelitesummary;
pub mod veolcity;

#[allow(unused_imports)]
use self::{
    dateandtime::DateAndTime, fixwithaltitude::FixWithAltitude, ignoreme::IgnoreMe,
    latlongdata::LatLongData, navsat::NavSatSystem, positionerror::PositionError,
    recmin::RecommendedMinimum, satellitedetail::SatellitesInView,
    sattelitesummary::SatelliteSummary, veolcity::Velocity,
};
use super::tools::test_line_crc;
use std::fmt::Display;

#[allow(unused)]
#[derive(Debug, Eq, Hash, PartialEq, PartialOrd, Ord)]
pub enum NavigationTalkers {
    Galileo,
    BeiDou,
    Gps,
    Glonass,
    Gnss,
    Qzss,
}

impl Display for NavigationTalkers {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::Galileo => "Galileo",
            Self::BeiDou => "BeiDou",
            Self::Gps => "GPS",
            Self::Glonass => "GLONASS",
            Self::Gnss => "GNSS",
            Self::Qzss => "QZSS",
        };

        write!(f, "{}", message)
    }
}

#[derive(Debug)]
pub enum FixQuality {
    NoFix,
    GpsFix,
    DgpsFix,
    PpsFix,
    IntRTK,
    FloatRtk,
    DeadReckoning,
    Manual,
    Simulation,
}

impl From<u8> for FixQuality {
    fn from(v: u8) -> Self {
        match v {
            1 => Self::GpsFix,
            2 => Self::DgpsFix,
            3 => Self::PpsFix,
            4 => Self::IntRTK,
            5 => Self::FloatRtk,
            6 => Self::DeadReckoning,
            7 => Self::Manual,
            8 => Self::Simulation,
            _ => Self::NoFix,
        }
    }
}

impl From<char> for FixQuality {
    fn from(v: char) -> Self {
        match v {
            'A' => Self::GpsFix,
            'D' => Self::DgpsFix,
            'P' => Self::PpsFix,
            'R' => Self::IntRTK,
            'F' => Self::FloatRtk,
            'E' => Self::DeadReckoning,
            'M' => Self::Manual,
            'S' => Self::Simulation,
            _ => Self::NoFix,
        }
    }
}

impl Display for FixQuality {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let message = match self {
            Self::NoFix => "Invalid",
            Self::GpsFix => "GpsFix",
            Self::DgpsFix => "DgpsFix",
            Self::PpsFix => "PpsFix",
            Self::IntRTK => "Integer RealTimeKinematic",
            Self::FloatRtk => "Float RealTimeKinematic",
            Self::DeadReckoning => "DeadReckoning",
            Self::Manual => "Manual",
            Self::Simulation => "Simulation",
        };

        write!(f, "{}", message)
    }
}

pub trait NmeaLine: Display {
    fn parse(
        type_str: &str,
        line: &str,
        parts: &[&str],
        uid: Option<String>,
    ) -> Result<Self, NmeaParseFailure>
    where
        Self: Sized;

    // fn hidden(&self) -> bool {
    //     false
    // }

    fn get_uid(&self) -> Option<String>;
}

// #[derive(Debug)]
// pub struct NmeaData {}

#[allow(unused)]
pub struct NmeaParseFailure {
    pub status: String,
    pub line_type: String,
    pub message: String,
    pub raw_line: Option<String>,
}

pub struct NmeaParseTinyFailure {
    pub message: String,
}

pub fn parse_line(input: &str) -> Result<Box<dyn NmeaLine>, NmeaParseFailure> {
    if !(input.contains("$") && input.contains(",")) {
        let m = String::from("Improper NMEA sentence");
        return Err(NmeaParseFailure {
            status: m.clone(),
            line_type: String::from("UNK"),
            message: m.clone(),
            raw_line: Some(input.to_string()),
        });
    }
    let raw_line = input.split("$").collect::<Vec<&str>>();
    let uid = match raw_line[0].is_empty() {
        false => {
            let ru = raw_line[0];
            let n = ru.len();
            Some(ru[..(n - 1)].into())
        }
        true => None,
    };
    let line_wcrc = raw_line[1].split("*").collect::<Vec<&str>>();
    let line = line_wcrc[0];
    let crc = line_wcrc[1];
    let type_str = &line[2..5];

    if line.len() > 1 {
        test_line_crc(type_str, line, crc)?;
    }

    let parts = line.split(",").collect::<Vec<&str>>();

    match type_str {
        "GGA" => Ok(Box::new(FixWithAltitude::parse(
            type_str, line, &parts, uid,
        )?)),
        // "GLL" => Ok(Box::new(LatLongData::parse(type_str, line, &parts, uid)?)),
        // "GNS" => Ok(Box::new(NavSatSystem::parse(type_str, line, &parts, uid)?)),
        // "GSA" => Ok(Box::new(SatelliteSummary::parse(
        //     type_str, line, &parts, uid,
        // )?)),
        // "GST" => Ok(Box::new(PositionError::parse(type_str, line, &parts, uid)?)),
        // "GSV" => Ok(Box::new(SatellitesInView::parse(
        //     type_str, line, &parts, uid,
        // )?)),
        // "RMC" => Ok(Box::new(RecommendedMinimum::parse(
        //     type_str, line, &parts, uid,
        // )?)),
        // "TXT" => Ok(Box::new(IgnoreMe::parse(type_str, line, &parts, uid)?)),
        "VTG" => Ok(Box::new(Velocity::parse(type_str, line, &parts, uid)?)),
        // "ZDA" => Ok(Box::new(DateAndTime::parse(type_str, line, &parts, uid)?)),
        _ => Err(NmeaParseFailure {
            status: "TODO".to_string(),
            line_type: String::from(type_str),
            message: line.to_string(),
            raw_line: Some(line.to_string()),
        }),
    }
}
