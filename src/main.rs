mod nmea;

use crate::nmea::types::parse_line;
use log::{debug, info, trace, warn};
use std::error::Error;
use std::net::SocketAddr;
use std::{env, io};
use tokio::net::UdpSocket;

struct Server {
    socket: UdpSocket,
    buf: Vec<u8>,
    to_send: Option<(usize, SocketAddr)>,
}

impl Server {
    async fn run(self) -> Result<(), io::Error> {
        let Server {
            socket,
            mut buf,
            mut to_send,
        } = self;

        loop {
            if let Some((size, peer)) = to_send {
                // Proper NMEA line will be at least six bytes long, and
                // netcat emits single byte test messages when using UDP
                if size > 6 {
                    if let Ok(text) = String::from_utf8(buf[..size].to_vec()) {
                        for line in text.trim().split('\n') {
                            trace!(target: "main::nmea-notice", "{}", line);
                            if let Ok(nmea_line) = parse_line(line) {
                                let uid = nmea_line.get_uid().unwrap_or_default();
                                info!(target: "main::nmea-notice", "{:8} -> {}", uid, nmea_line);
                            } else {
                                debug!(target: "main::nmea-unhandled", "{}", line);
                            }
                        }
                    } else {
                        warn!(target: "main::nmea-notice", "Garbage from {} ({} bytes)", peer, size);
                    }
                }
            }

            to_send = Some(socket.recv_from(&mut buf).await?);
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    init_env_and_log();

    let addr = env::args()
        .nth(1)
        .unwrap_or_else(|| "0.0.0.0:44444".to_string());

    let socket = UdpSocket::bind(&addr).await?;
    info!(target: "main", "Listening on: {}", socket.local_addr()?);

    let server = Server {
        socket,
        buf: vec![0; 1024],
        to_send: None,
    };

    // This starts the server task.
    server.run().await?;

    Ok(())
}

fn init_env_and_log() {
    dotenv::dotenv().ok();
    env_logger::Builder::from_default_env().init();
}
